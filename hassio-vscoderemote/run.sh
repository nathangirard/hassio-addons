#!/bin/bash

# SIGTERM-handler this funciton will be executed when the container receives the SIGTERM signal (when stopping)
reset_interfaces(){
    ifdown $INTERFACE
    sleep 1
    ip link set $INTERFACE down
    ip addr flush dev $INTERFACE
}

term_handler(){
    echo "Resseting interfaces"
    reset_interfaces
    echo "Stopping..."
    exit 0
}

# Setup signal handlers
trap 'term_handler' SIGTERM

echo "Starting..."

CONFIG_PATH=/data/options.json

SSID=$(jq --raw-output ".ssid" $CONFIG_PATH)
WPA_PASSPHRASE=$(jq --raw-output ".wpa_passphrase" $CONFIG_PATH)
CHANNEL=$(jq --raw-output ".channel" $CONFIG_PATH)
ADDRESS=$(jq --raw-output ".address" $CONFIG_PATH)
SUBNET=$(jq --raw-output ".subnet" $CONFIG_PATH)
NETMASK=$(jq --raw-output ".netmask" $CONFIG_PATH)
BROADCAST=$(jq --raw-output ".broadcast" $CONFIG_PATH)
INTERFACE=$(jq --raw-output ".interface" $CONFIG_PATH)
LEASESTART=$(jq --raw-output ".leasestart" $CONFIG_PATH)
LEASEEND=$(jq --raw-output ".leaseend" $CONFIG_PATH)

# Enforces required env variables
required_vars=(SSID WPA_PASSPHRASE CHANNEL ADDRESS NETMASK BROADCAST)
for required_var in "${required_vars[@]}"; do
    if [[ -z ${!required_var} ]]; then
        echo >&2 "Error: $required_var env variable not set."
        exit 1
    fi
done


INTERFACES_AVAILABLE="$(ifconfig -a | grep wl | cut -d ' ' -f '1')"
UNKNOWN=true

if [[ -z $INTERFACE ]]; then
        echo >&2 "Network interface not set. Please set one of the available:"
        echo >&2 "${INTERFACES_AVAILABLE}"
        exit 1
fi

for OPTION in  ${INTERFACES_AVAILABLE}; do
    if [[ ${INTERFACE} == ${OPTION} ]]; then
        UNKNOWN=false
    fi 
done

if [[ $UNKNOWN == true ]]; then
        echo >&2 "Unknown network interface ${INTERFACE}. Please set one of the available:"
        echo >&2 "${INTERFACES_AVAILABLE}"
        exit 1
fi

echo "Set nmcli managed no"
nmcli dev set $INTERFACE managed no

echo "Network interface set to ${INTERFACE}"

# Setup hostapd.conf
HCONFIG="/hostapd.conf"

echo "Setup hostapd ..."
echo "ssid=${SSID}" >> ${HCONFIG}
echo "wpa_passphrase=${WPA_PASSPHRASE}" >> ${HCONFIG}
echo "channel=${CHANNEL}" >> ${HCONFIG}
echo "interface=${INTERFACE}" >> ${HCONFIG}
echo "" >> ${HCONFIG}

# Setup dhcpd.conf
DCONFIG="/dhcpd.conf"
echo "Setup dhcpd ..."
echo "authoritative;" > ${DCONFIG}
echo "subnet ${SUBNET} netmask ${NETMASK} {" >> ${DCONFIG}
echo "	range ${LEASESTART} ${LEASEEND};" >> ${DCONFIG}
echo "	option broadcast-address ${BROADCAST};" >> ${DCONFIG}
echo "	option routers ${ADDRESS};" >> ${DCONFIG}
echo "	default-lease-time 600;" >> ${DCONFIG}
echo "	max-lease-time 7200;" >> ${DCONFIG}
echo "	option domain-name-servers ${ADDRESS};" >> ${DCONFIG}
echo "}" >> ${DCONFIG}

# Setup dnsmasq.conf
#DCONFIG="/dnsmasq.conf"
#echo "interface=lo,${INTERFACE}" > ${DCONFIG}
#echo "bind-interfaces" >> ${DCONFIG}
#echo "no-resolv" >> ${DCONFIG}
#echo "local=/local/" >> ${DCONFIG}
#echo "domain=local" >> ${DCONFIG}
#echo "server=8.8.8.8" >> ${DCONFIG}
#echo "server=8.8.4.4" >> ${DCONFIG}
#echo "dhcp-range=${LEASESTART},${LEASEEND},${NETMASK},1h" >> ${DCONFIG}

# Setup interface
IFFILE="/etc/network/interfaces"

echo "Setup interface ..."
echo "" > ${IFFILE}
echo "iface ${INTERFACE} inet static" >> ${IFFILE}
echo "  address ${ADDRESS}" >> ${IFFILE}
echo "  netmask ${NETMASK}" >> ${IFFILE}
echo "  broadcast ${BROADCAST}" >> ${IFFILE}
echo "" >> ${IFFILE}

echo "Resseting interfaces"
reset_interfaces
ifup $INTERFACE
sleep 1

echo "Starting HostAP daemon ..."
#dhcpd -4 -f -cf ${DCONFIG} -d ${INTERFACE} &
#dnsmasq -C ${DCONFIG} &
hostapd -d ${HCONFIG} & wait ${!}
